
from ev3dev.ev3 import *

import time



def checkColor(c_value):

    if c_value!=1 and c_value!=2 and c_value!=3 and c_value!=0 and c_value!=3 and c_value!=4 and c_value!=5 and c_value!=7:

        return True

    return False

def forward(motor1,motor2):

    motor1.run_forever(speed_sp=100)

    motor2.run_forever(speed_sp=100)

def forward_time(motor1,motor2,c,time,speed):

    motor1.run_timed(time_sp=time,speed_sp = speed)

    motor2.run_timed(time_sp=time,speed_sp = speed)

    found = False;

    count = 0 

    while(motor1.state[0]=='running' and motor2.state[0]=='running'):

        c_value = c.value();

        if(checkColor(c_value)==False):

                motor1.stop(stop_action='hold')

                motor2.stop(stop_action='hold')	

                found = True

                count = count + 1

                if count>1:

                     break

    return found

def stop(motor1,motor2):

    motor1.stop(stop_action="hold")

    motor2.stop(stop_action="hold")

def checkLeftAngle(motor1,motor2,c,time,speed):

    motor1.stop(stop_action="hold")

    motor2.run_timed(time_sp=time, speed_sp=speed)

    found = False;

    count = 0 

    while(motor2.state[0]=='running'):

        c_value = c.value();

        #print("left",c_value);

        if(checkColor(c_value)==False):

                motor2.stop(stop_action='hold')	

                count = count + 1

                found = True

                print("Left found")

                break

    return found

def checkRightAngle(motor1,motor2,c,time,speed):

    motor1.run_timed(time_sp=time, speed_sp=speed)

    motor2.stop(stop_action="hold")

    found = False

    count = 0

    while(motor1.state[0]=='running'):

        c_value = c.value()

        #print("Right",c_value)

        if(checkColor(c_value)==False):

                motor1.stop(stop_action='hold')

                count = count + 1

                print("Right found")

                found = True

                break

    return found

def turnLeft(m1,m2,not_found_count,lastLeft,lastRight,c,found):

    if(found==False and m1.state[0]!='running' and m2.state[0]!='running' and not_found_count==0):

                print("Last Left",lastLeft);

                print("Last Right",lastRight);

                found = checkLeftAngle(m1,m2,c,2900,100)

                if found==True:

                      lastLeft =  1

                      lastRight = 0

                else:

                      found = checkLeftAngle(m1,m2,c,2600,-100)

                      lastLeft = 0

    return found,lastLeft,lastRight

def turnRight(m1,m2,not_found_count,lastLeft,lastRight,c,found):

    if(found==False and m1.state[0]!='running' and m2.state[0]!='running' and not_found_count==0):

                print("Last Left",lastLeft)

                print("Last Right",lastRight);

                found = checkRightAngle(m1,m2,c,2900,100)

                if found==True:

                     lastRight = 1

                     lastLeft = 0

                else:

                     found = checkRightAngle(m1,m2,c,2600,-100)    

                     lastRight = 0

    return found,lastLeft,lastRight

def movement(c,m1,m2):

    lastLeft = 0

    lastRight =  0

    not_found_count = 0
    angle = 0

    while(1):

        forward(m1,m2)

        out_of_path = False

        total_count = 0

        while(1):

            c_value = c.value()

            #print("Forward",c_value)

            if(checkColor(c_value)==True):

                total_count = total_count + 1

                if total_count>=1:

                  out_of_path = True

                  break

        if out_of_path==True:

            stop(m1,m2)

            found = False;

            if lastRight==1:

               print("Last was right.Taking Left")            	

               found,lastLeft,lastRight =  turnLeft(m1,m2,not_found_count,lastLeft,lastRight,c,found)

               found,lastLeft,lastRight =  turnRight(m1,m2,not_found_count,lastLeft,lastRight,c,found)

            else:

               print("Last was left.Taking Right")

               found,lastLeft,lastRight =  turnRight(m1,m2,not_found_count,lastLeft,lastRight,c,found)

               found,lastLeft,lastRight =  turnLeft(m1,m2,not_found_count,lastLeft,lastRight,c,found)

                   

            if(found==False and m1.state[0]!='running' and m2.state[0]!='running'):

                found = checkRightAngle(m1,m2,c,angle,100)

                found = forward_time(m1,m2,c,4000,150)

                if found == False:

                    found = forward_time(m1,m2,c,4000,-150)

                found = checkRightAngle(m1,m2,c,angle,-100)

            if(found==False and m1.state[0]!='running' and m2.state[0]!='running'):

                found = checkLeftAngle(m1,m2,c,angle,100)

                found = forward_time(m1,m2,c,4000,150)

                if found == False:

                    found = forward_time(m1,m2,c,4000,-150)

                found = checkLeftAngle(m1,m2,c,angle,-100)        

            if(found==False):

                #found = forward_time(m1,m2,c,400,80)

                not_found_count = not_found_count + 1
                angle = angle + 1200

                if not_found_count>=7:

                     break

            else:
                angle = 0

                not_found_count = 0

              		

if __name__ == "__main__":

    c = ColorSensor()

    m1 = LargeMotor('outB')

    m2 = LargeMotor('outC')

    movement(c,m1,m2);
